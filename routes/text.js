var express = require('express');
var router = express.Router();


// create application/json parser

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

router.post('/', function(req, res) {
    if (req.body.text) {
        return res.status(200).send(req.body.text);
    }
    return res.status(500).send({'error':'Text cannot be left blank.'});
});

module.exports = router;