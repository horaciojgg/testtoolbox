const request = require('supertest');
const app = require('../app');
let chai = require('chai');
const { expect } = require('chai');
let should = chai.should();

describe('App', function() {
  it('has the default page', function(done) {
    request(app)
      .get('/')
      .expect(/Welcome to Express/, done);
  });
}); 

describe('/text', function() {
  it('Returns the same text posted to the /text endpoint.', function(done) {
    request(app)
      .post('/text')
      .send({"text": "textodepruebas"})
      .end((err, res) => {
        res.status.should.equal(200);
        res.text.should.equal("textodepruebas");
        done();
      });
  });

  it('Should return error if request does not have a text key', function(done) {
    request(app)
      .post('/text')
      .send({"textErroneo": "textodepruebas"})
      .end((err, res) => {
        res.status.should.equal(500);
        done();
      });
  });
}); 
