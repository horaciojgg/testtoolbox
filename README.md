
### Pre-requisites
    All you need to have is Node in version 8 or higher.

### Setup
    To get started with the project, first excecute the command "npm install" to get all your dependencies.

### Instructions
    In order to run this projects, open a new terminal and execute the command "npm run start". To run the unit tests, execute "npm run test". 